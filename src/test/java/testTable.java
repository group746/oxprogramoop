/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.saharat.oxprogramoop.Player;
import com.saharat.oxprogramoop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Sakonsupa
 */
public class testTable {

    public testTable() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void TestRow1ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void TestRow2ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    @Test
    public void TestRow1ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
}
